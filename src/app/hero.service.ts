import { Injectable } from '@angular/core';
import { Hero } from './hero';
import { HEROES } from './mock-heroes';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  constructor(private messageService:MessageService) { }

  getHeroes(): Observable<Hero[]> {
    if(HEROES){
      this.messageService.add('Heros Fetched properly');
      }
      else{
        this.messageService.add('No Heroes Found');
      }
    return of(HEROES);
    
  }
}
